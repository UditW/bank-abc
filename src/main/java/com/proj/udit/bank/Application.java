package com.proj.udit.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by uwadhwa on 8/25/17.
 */
@SpringBootApplication
public class Application {
    public static void main(String argv[]){
        SpringApplication.run(Application.class,argv);
    }
}
