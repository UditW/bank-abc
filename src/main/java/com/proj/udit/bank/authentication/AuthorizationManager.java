package com.proj.udit.bank.authentication;

import com.proj.udit.bank.domain.EmployeeServiceTypes;
import com.proj.udit.bank.domain.User;
import com.proj.udit.bank.domain.UserCategory;
import com.proj.udit.bank.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by uwadhwa on 8/27/17.
 */
@Component
public class AuthorizationManager {

    private final UserRepository userRepository;

    @Autowired
    public AuthorizationManager(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static boolean isDenied(EmployeeServiceTypes type) {

        List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        for (SimpleGrantedAuthority authority : authorities) {
            if (authority.getAuthority().equalsIgnoreCase(type.name())) {
                return false;
            }
        }

        return true;
    }

    public UserCategory getUserCategory(String loginName) {
        User user = userRepository.getCustomer(loginName);
        if (user != null) {
            return user.getCategory();
        }
        return UserCategory.NEW;
    }
}
