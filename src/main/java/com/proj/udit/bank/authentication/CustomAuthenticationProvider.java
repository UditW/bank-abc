package com.proj.udit.bank.authentication;

import com.google.common.hash.Hashing;
import com.proj.udit.bank.domain.User;
import com.proj.udit.bank.domain.UserCategory;
import com.proj.udit.bank.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by uwadhwa on 8/26/17.
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {


    private final UserRepository userRepository;

    @Autowired
    CustomAuthenticationProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // http://www.baeldung.com/spring-security-login to get custom auth working read about setting the loginProcessingUrl

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {


        final User user = userRepository.getCustomer(authentication.getName());

        if (user != null) {
            if (user.getHashedPassword().equals(Hashing.sha256().hashString(authentication.getCredentials().toString(), StandardCharsets.UTF_8).toString())) {
                return new UsernamePasswordAuthenticationToken(
                        authentication.getName(), authentication.getCredentials().toString(), getAuthoritiesAsPerRole(user.getCategory()));
            } else {
                throw new BadCredentialsException("Mismatch in loginName or password.");
            }
        }

        return null;
    }

    private List<SimpleGrantedAuthority> getAuthoritiesAsPerRole(UserCategory category) {
        switch (category) {
            case REGULAR:
            case NEW:
                return DefinedAuthorities.REGULAR_CUSTOMER_AUTHORITIES;
            case VALUED:
                return DefinedAuthorities.VALUED_CUSTOMER_AUTHORITIES;
            case EMPLOYEE:
                return DefinedAuthorities.EMPLOYEE_AUTHORITIES;
            case MANAGER:
                return DefinedAuthorities.MANAGER_AUTHORITIES;
        }
        return new ArrayList<>();
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}