package com.proj.udit.bank.authentication;

import com.proj.udit.bank.domain.EmployeeServiceTypes;
import com.proj.udit.bank.domain.ServiceTypes;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uwadhwa on 8/27/17.
 */
@Component
public class DefinedAuthorities {

    public static final List<SimpleGrantedAuthority> REGULAR_CUSTOMER_AUTHORITIES = new ArrayList<>();
    public static final List<SimpleGrantedAuthority> VALUED_CUSTOMER_AUTHORITIES = new ArrayList<>();
    public static final List<SimpleGrantedAuthority> EMPLOYEE_AUTHORITIES = new ArrayList<>();
    public static final List<SimpleGrantedAuthority> MANAGER_AUTHORITIES = new ArrayList<>();

    static {
        for (ServiceTypes type : ServiceTypes.values()) {
            REGULAR_CUSTOMER_AUTHORITIES.add(new SimpleGrantedAuthority(type.name()));
            VALUED_CUSTOMER_AUTHORITIES.add(new SimpleGrantedAuthority(type.name()));
        }

        for (EmployeeServiceTypes type : EmployeeServiceTypes.getEmployeeServices()) {
            EMPLOYEE_AUTHORITIES.add(new SimpleGrantedAuthority(type.name()));
        }

        for (EmployeeServiceTypes type : EmployeeServiceTypes.getManagerServices()) {
            MANAGER_AUTHORITIES.add(new SimpleGrantedAuthority(type.name()));
        }
    }

}
