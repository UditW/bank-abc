package com.proj.udit.bank.configuration;

import com.mongodb.*;
import com.proj.udit.bank.Application;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.converters.Converters;
import org.mongodb.morphia.mapping.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;

@Configuration
public class MongoConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoConfiguration.class);


    @Value("${mongodb.connection.host:localhost}")
    private String dbHost;

    @Value("${mongodb.connection.port:27017}")
    private int dbPort;


    @Value("${mongodb.connection.connections_per_host:100}")
    private int connectionsPerHost;

    @Value("${mongodb.connection.thread_multiplier:10}")
    private int threadMultiplier;

    @Value("${mongodb.connection.connection_timeout:10000}")
    private int connectionTimeout;

    @Value("${mongodb.connection.socket_timeout:10000}")
    private int socketTimeout;

    @Value("${mongodb.connection.socket_keepalive:true}")
    private boolean socketKeepAlive;

    @Value("${mongodb.connection.autoconnectretry:false}")
    private boolean autoConnectRetry;

    @Value("${mongodb.connection.maxautoconnectretrytime:0}")
    private int maxAutoConnectRetryTime;

    //if this is set to 0 or -1
    @Value("${mongodb.connection.write_concern:1}")
    private int writeConcern;

    @Value("${mongodb.connection.write_timeout:0}")
    private int writeTimeout;

    @Value("${mongodb.connection.write_fsync:false}")
    private boolean writeFsync;

    @Value("${mongodb.connection.db:bank}")
    private String appDbName;


    private MongoClient mongo() throws UnknownHostException, MongoException {
        MongoClient m;

        MongoClientOptions options = new MongoClientOptions.Builder().
                connectionsPerHost(connectionsPerHost).
                threadsAllowedToBlockForConnectionMultiplier(threadMultiplier).
                connectTimeout(connectionTimeout).
                socketTimeout(socketTimeout).
                socketKeepAlive(socketKeepAlive).
                readPreference(ReadPreference.primary()).
                writeConcern(new WriteConcern(writeConcern)).build();


        LOGGER.info("MONGODB: Instantiating MongoDB bean with options " + options.toString());

        m = new MongoClient(new ServerAddress(dbHost, dbPort), options);


        LOGGER.info("MONGODB: MongoDB bean created successfully");
        return m;
    }

    private static Morphia morphia() {

        Morphia morphia = new Morphia();
        morphia.mapPackage(Application.class.getPackage().getName(), true);

        return morphia;
    }


    @Bean(name = "datastore")
    public Datastore morphiaDatastore() throws UnknownHostException, MongoException {
        Datastore ds;
        Morphia morphia = morphia();

        MongoClient mongo = mongo();

        ds = morphia.createDatastore(mongo, appDbName);
        Mapper morphiaMapper = morphia.getMapper();
        addConverters(morphiaMapper.getConverters());
        ds.ensureCaps();

        return ds;

    }

    private static void addConverters(Converters converters) {
        //converters.addConverter(new ReviewChangeEventProcessingStatusTypeConverter());
        //converters.addConverter(new HotelReviewChangeNotificationProcessorTypeConverter());
    }
}
