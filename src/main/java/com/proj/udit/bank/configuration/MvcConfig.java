package com.proj.udit.bank.configuration;

/**
 * Created by uwadhwa on 8/25/17.
 */

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login-page").setViewName("loginPage");
        registry.addViewController("/new-user").setViewName("newuser");
        registry.addViewController("/emp").setViewName("employeePortal");
        registry.addViewController("/token").setViewName("token");
    }

}
