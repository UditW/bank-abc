package com.proj.udit.bank.controllers;

import com.proj.udit.bank.domain.Counter;
import com.proj.udit.bank.repository.CounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uwadhwa on 8/27/17.
 */
@Controller
@RequestMapping("/counters")
public class CountersController {

    private final CounterRepository counterRepository;

    @Autowired
    public CountersController(CounterRepository counterRepository) {
        this.counterRepository = counterRepository;
    }


    @GetMapping("/display")
    public String listAllCounters(Model model) {

        List<Counter> list = counterRepository.getCounters(0, 1, 2);

        model.addAttribute("counters", list);

        return "/counter";

    }

}
