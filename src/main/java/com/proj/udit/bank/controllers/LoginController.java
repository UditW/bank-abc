package com.proj.udit.bank.controllers;

import com.google.common.hash.Hashing;
import com.proj.udit.bank.domain.*;
import com.proj.udit.bank.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by uwadhwa on 8/25/17.
 */
@Controller
public class LoginController {

    private final UserRepository userRepository;

    @Autowired
    public LoginController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public String login(@ModelAttribute("credentials") Credentials credentials, Model model) {

        return "/services";
    }

    @GetMapping("/name-exists")
    public boolean loginNameExists(@RequestParam String loginName) {
        return userRepository.loginNameExists(loginName);
    }

    @PostMapping("/create")
    public String createUser(@RequestParam String firstname, @RequestParam String loginName, @RequestParam String lastname, @RequestParam String password,
                             @RequestParam String phone, @RequestParam String house, @RequestParam String street,
                             @RequestParam String city, @RequestParam String state, @RequestParam long pin) {

        User.UserInfoBuilder builder = new User.UserInfoBuilder();
        Address address = new Address(house, street, city, state, pin);
        User user = builder.name(firstname).lastName(lastname)
                .hashedPassword(Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString())
                .phoneNo(phone).loginName(loginName).address(address).build();

        try {
            userRepository.addUser(user);
        } catch (Exception e) {
            return "/error";
        }

        return "loginPage";
    }


    private RequestContext initRequestContext(User user) {
        RequestContext context = new RequestContext();
        context.setCategory(user.getCategory());
        context.setLoginName(user.getLoginName());
        return context;
    }
}
