package com.proj.udit.bank.controllers;

import com.proj.udit.bank.domain.ServiceTypes;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uwadhwa on 8/25/17.
 */
@Controller
public class ServicesController {


    // https://spring.io/guides/gs/serving-web-content/
    // https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-developing-web-applications.html
    // https://spring.io/guides/gs/securing-web/
    // http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html#standard-expression-syntax

    // mvn spring-boot:run -Drun.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"


    @RequestMapping("/services")
    public String test(Model model) {
        getServicesBasedOnAuthorization(model);
        return "services";
    }


    private void getServicesBasedOnAuthorization(Model model) {
        List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        model.addAttribute("services", authorities);
    }
}
