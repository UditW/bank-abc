package com.proj.udit.bank.controllers;

import com.proj.udit.bank.authentication.AuthorizationManager;
import com.proj.udit.bank.domain.*;
import com.proj.udit.bank.repository.TokenRepository;
import com.proj.udit.bank.repository.UserRepository;
import com.proj.udit.bank.utils.TokenPreprocessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by uwadhwa on 8/25/17.
 */
@Controller
public class TokenController {


    // https://spring.io/guides/gs/serving-web-content/
    // https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-developing-web-applications.html
    // https://spring.io/guides/gs/securing-web/
    // http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html#standard-expression-syntax

    // mvn spring-boot:run -Drun.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"


    private final TokenRepository tokenRepository;
    private final static Logger LOGGER = LoggerFactory.getLogger(TokenController.class);

    @Autowired
    public TokenController(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @PostMapping("/register-request")
    public String registerUserRequest(@RequestParam("service") String[] services, Model model) {

        final long tokenNum = this.tokenRepository.getNewTokeNumber();

        model.addAttribute("tokenNum", tokenNum);
        model.addAttribute("services", services);
        persistTokenDetails(tokenNum, services);

        return "/token";
    }

    @GetMapping("/token-queue")
    public String getTokenQueueCount(Model model) {
        if (AuthorizationManager.isDenied(EmployeeServiceTypes.TOKEN_QUEUE_COUNT))
            return "/error";
        else
            model.addAttribute("count", TokenPreprocessor.getTokenCountInQueue());

        return "/tokenqueue";
    }


    @PutMapping("/reset-token")
    public String resetTokenCounter() {


        return null;
    }


    private void persistTokenDetails(long tokenNumber, String[] services) {
        Token token = new Token();
        LocalDateTime now = LocalDateTime.now();
        token.setIssueTime(now);
        token.setExpireTime(now.plusDays(3));
        token.setTokenId(tokenNumber);
        // TODO: Add a null check here.
        token.setLoginName(getUserDetails());

        token.setRequestedServices(getRequestedServiceList(services));
        token.setState(TokenState.QUEUED);

        tokenRepository.persistToken(token);
        try {
            TokenPreprocessor.enqueueAToken(tokenNumber);
        } catch (Exception e) {
            LOGGER.error(" Errors seen while adding new tokens to Queue.", e);
        }

    }

    private String getUserDetails() {
        return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    private List<ServiceTypes> getRequestedServiceList(String[] services) {
        List<ServiceTypes> serviceTypes = new ArrayList<>();

        Arrays.stream(services).forEach((s) -> {
            ServiceTypes type = ServiceTypes.getServiceType(s);
            if (type != null) {
                serviceTypes.add(type);
            }
        });
        return serviceTypes;
    }


}
