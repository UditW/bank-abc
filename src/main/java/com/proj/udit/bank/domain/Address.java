package com.proj.udit.bank.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by uwadhwa on 8/24/17.
 */
public class Address {

    @Property("h")
    @NotEmpty
    private String houseNo;

    @Property("sAdr")
    private String streetAddress;

    @Property("cnm")
    @NotEmpty
    private String cityName;

    @Property("st")
    private String state;

    @Property("pin")
    @NotEmpty
    private long pinCode;

    public Address(){
        //For Morphia
    }

    public Address(String houseNo, String streetAddress, String cityName, String state, long pinCode){
        this.houseNo = houseNo;
        this.cityName = cityName;
        this.streetAddress = streetAddress;
        this.state = state;
        this.pinCode = pinCode;
    }

    public Address(String houseNo, String cityName, String state, long pinCode){
        this.houseNo = houseNo;
        this.cityName = cityName;
        this.state = state;
        this.pinCode = pinCode;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getPinCode() {
        return pinCode;
    }

    public void setPinCode(long pinCode) {
        this.pinCode = pinCode;
    }
}
