package com.proj.udit.bank.domain;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by uwadhwa on 8/27/17.
 */
@Entity(noClassnameStored = true)
public class Comments {

    @Property("cno")
    private int counterNo;

    @Property("com")
    private String comment;

    @Property("eid")
    private long employeeId;

    @Property("asvc")
    private ServiceTypes associatedService;

    public int getCounterNo() {
        return counterNo;
    }

    public void setCounterNo(int counterNo) {
        this.counterNo = counterNo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public ServiceTypes getAssociatedService() {
        return associatedService;
    }

    public void setAssociatedService(ServiceTypes associatedService) {
        this.associatedService = associatedService;
    }
}
