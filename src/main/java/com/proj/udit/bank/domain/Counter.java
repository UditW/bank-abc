package com.proj.udit.bank.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by uwadhwa on 8/26/17.
 */
@Entity(noClassnameStored = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Counter {

    @Property("svs")
    private List<ServiceTypes> services;

    @Property("eid")
    private long employeeId;

    @Property("cnt")
    private long currentCount;

    @Property("catL")
    private List<UserCategory> catersTo;

    @Embedded("qCst")
    private List<QueuedCustomer> queuedCustomers;

    @Id
    private int id;

    @Property("st")
    private LocalDateTime startTime;

    @Property("et")
    private LocalDateTime endTime;

    public List<ServiceTypes> getServices() {
        return services;
    }

    public void setServices(List<ServiceTypes> services) {
        this.services = services;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public long getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(long currentCount) {
        this.currentCount = currentCount;
    }

    public List<UserCategory> getCatersTo() {
        return catersTo;
    }

    public void setCatersTo(List<UserCategory> catersTo) {
        this.catersTo = catersTo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public List<QueuedCustomer> getQueuedCustomers() {
        return queuedCustomers;
    }

    public void setQueuedCustomers(List<QueuedCustomer> queuedCustomers) {
        this.queuedCustomers = queuedCustomers;
    }
}
