package com.proj.udit.bank.domain;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by uwadhwa on 8/25/17.
 */
public class Credentials {

    @NotEmpty
    private String userName;

    @NotEmpty
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
