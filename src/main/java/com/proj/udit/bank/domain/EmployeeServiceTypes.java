package com.proj.udit.bank.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by uwadhwa on 8/25/17.
 */
public enum EmployeeServiceTypes {
    RESET_TOKEN(UserCategory.MANAGER),
    CHECK_COUNTER_STATE(UserCategory.EMPLOYEE),
    ADD_NEW_COUNTER(UserCategory.REGULAR),
    REMOVE_COUNTER(UserCategory.MANAGER),
    ADD_NEW_EMPLOYEE(UserCategory.MANAGER),
    TOKEN_QUEUE_COUNT(UserCategory.EMPLOYEE);


    private UserCategory category;

    public UserCategory getCategory() {
        return category;
    }

    EmployeeServiceTypes(UserCategory category) {
        this.category = category;
    }

    public static List<EmployeeServiceTypes> getEmployeeServices() {
        List<EmployeeServiceTypes> serviceTypes = new ArrayList<>();
        for (EmployeeServiceTypes type : EmployeeServiceTypes.values()) {
            if (type.getCategory().equals(UserCategory.EMPLOYEE)) {
                serviceTypes.add(type);
            }
        }

        return serviceTypes;
    }

    public static List<EmployeeServiceTypes> getManagerServices() {
        return Arrays.asList(EmployeeServiceTypes.values());
    }
}
