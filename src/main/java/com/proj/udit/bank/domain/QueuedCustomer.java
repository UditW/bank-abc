package com.proj.udit.bank.domain;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by uwadhwa on 8/27/17.
 */
@Entity(noClassnameStored = true)
public class QueuedCustomer {

    @Property("tkn")
    private long tokenNum;

    @Property("svc")
    private ServiceTypes type;

    public long getTokenNum() {
        return tokenNum;
    }

    public void setTokenNum(long tokenNum) {
        this.tokenNum = tokenNum;
    }

    public ServiceTypes getType() {
        return type;
    }

    public void setType(ServiceTypes type) {
        this.type = type;
    }
}
