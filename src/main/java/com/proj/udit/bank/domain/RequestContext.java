package com.proj.udit.bank.domain;

/**
 * Created by uwadhwa on 8/26/17.
 */
public class RequestContext {

    private String loginName;

    private UserCategory category;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public UserCategory getCategory() {
        return category;
    }

    public void setCategory(UserCategory category) {
        this.category = category;
    }
}
