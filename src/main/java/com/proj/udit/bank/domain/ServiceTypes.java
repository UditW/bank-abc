package com.proj.udit.bank.domain;

/**
 * Created by uwadhwa on 8/25/17.
 */
public enum ServiceTypes {
    WITHDRAWL,
    NEW_ACCOUNT,
    DEPOSIT,
    KYC_SUBMISSION,
    PASSBOOK_UPDATE,
    LOCKER_ACCESS;

    public static ServiceTypes getServiceType(String serviceName) {
        for (ServiceTypes svc : ServiceTypes.values()) {
            if (serviceName.equalsIgnoreCase(svc.name())) {
                return svc;
            }
        }
        return null;
    }
}
