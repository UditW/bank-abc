package com.proj.udit.bank.domain;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by uwadhwa on 8/24/17.
 */
@Entity(noClassnameStored = true)
public class Token {

    @Property("tkid")
    private long tokenId;

    @Property("ist")
    private LocalDateTime issueTime;

    @Property("ext")
    private LocalDateTime expireTime;

    @Property("st")
    private TokenState state;

    @Embedded("coms")
    private List<Comments> comments;

    @Property("u")
    private String loginName;

    @Property("rsvc")
    private List<ServiceTypes> requestedServices;

    public long getTokenId() {
        return tokenId;
    }

    public void setTokenId(long tokenId) {
        this.tokenId = tokenId;
    }

    public LocalDateTime getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(LocalDateTime issueTime) {
        this.issueTime = issueTime;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    public TokenState getState() {
        return state;
    }

    public void setState(TokenState state) {
        this.state = state;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String user) {
        this.loginName = user;
    }

    public List<ServiceTypes> getRequestedServices() {
        return requestedServices;
    }

    public void setRequestedServices(List<ServiceTypes> requestedServices) {
        this.requestedServices = requestedServices;
    }

    @Override
    public String toString() {
        return "Token{" +
                "tokenId=" + tokenId +
                ", issueTime=" + issueTime +
                ", expireTime=" + expireTime +
                ", state=" + state +
                ", comments=" + comments +
                ", loginName=" + loginName +
                ", requestedServices=" + requestedServices +
                '}';
    }
}
