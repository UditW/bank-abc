package com.proj.udit.bank.domain;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

/**
 * Created by uwadhwa on 8/26/17.
 */
@Entity(noClassnameStored = true)
public class TokenNumber {

    @Property("tnm")
    private long tokenNum;

    @Id
    private int id;

    public long getTokenNum() {
        return tokenNum;
    }

    public void setTokenNum(long tokenNum) {
        this.tokenNum = tokenNum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
