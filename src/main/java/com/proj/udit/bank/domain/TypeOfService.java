package com.proj.udit.bank.domain;

/**
 * Created by uwadhwa on 8/24/17.
 */
public enum TypeOfService {
    REGULAR,
    PREMIUM;
}
