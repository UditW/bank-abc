package com.proj.udit.bank.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.mongodb.morphia.annotations.*;

/**
 * Created by uwadhwa on 8/24/17.
 */
@Entity(noClassnameStored = true)
public class User {

    @Id
    private String id;

    @Property("lnm")
    @NotEmpty
    @Indexed(background = true,unique = true)
    private String loginName;

    @Property("nm")
    @NotEmpty
    private String name;

    @Property("ln")
    @NotEmpty
    private String lastName;

    @Property("pNo")
    @NotEmpty
    private String phoneNo;

    @Embedded("addr")
    @NotEmpty
    private Address address;

    @Property("cat")
    private UserCategory category;

    @Property("pw")
    @NotEmpty
    private String hashedPassword;


    public static class UserInfoBuilder {
        private String loginName;

        private String name;

        private String lastName;

        private String phoneNo;

        private Address address;

        private UserCategory category;

        private String hashedPassword;

        public UserInfoBuilder getBuilder() {
            return this;
        }

        public UserInfoBuilder loginName(String loginName) {
            this.loginName = loginName;
            return this;
        }

        public UserInfoBuilder name(String name) {
            this.name = name;
            return this;
        }

        public UserInfoBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserInfoBuilder phoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
            return this;
        }

        public UserInfoBuilder address(Address address) {
            this.address = address;
            return this;
        }

        public UserInfoBuilder hashedPassword(String hashedPassword) {
            this.hashedPassword = hashedPassword;
            return this;
        }

        public User build() {
            User user = new User();
            user.setAddress(this.address);
            user.setHashedPassword(this.hashedPassword);
            if (this.category == null)
                user.setCategory(UserCategory.MANAGER);
            else
                user.setCategory(this.category);
            user.setLoginName(this.loginName);
            user.setName(this.name);
            user.setLastName(this.lastName);
            user.setPhoneNo(this.phoneNo);
            return user;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public UserCategory getCategory() {
        return category;
    }

    public void setCategory(UserCategory category) {
        this.category = category;
    }
}
