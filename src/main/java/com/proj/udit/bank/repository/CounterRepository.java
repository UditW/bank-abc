package com.proj.udit.bank.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.proj.udit.bank.domain.Counter;
import com.proj.udit.bank.domain.QueuedCustomer;
import com.proj.udit.bank.domain.ServiceTypes;
import com.proj.udit.bank.domain.UserCategory;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.FindOptions;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.Sort;
import org.mongodb.morphia.query.UpdateOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

/**
 * Created by uwadhwa on 8/26/17.
 */
@Repository
public class CounterRepository {

    private final Datastore datastore;
    private static final Logger LOGGER = LoggerFactory.getLogger(CounterRepository.class);

    @Autowired
    public CounterRepository(Datastore datastore) {
        this.datastore = datastore;
        //bootstrapCounters();
    }

    private void bootstrapCounters() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(getClass().getResource("/bootstrap-counters").getFile()));
            String line = bufferedReader.readLine();
            while (line != null) {
                Counter counter = mapper.readValue(line, Counter.class);
                datastore.save(counter);
                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Error during the bootstrapping operation of counters.", e);
        }

    }

    public void assignACounter(UserCategory category, ServiceTypes type, long tokenNum) {
        Query<Counter> query = datastore.createQuery(Counter.class);
        query.field("catersTo").equal(category).field("services").equal(type).order(Sort.ascending("currentCount"));

        UpdateOperations<Counter> updateOperations = datastore.createUpdateOperations(Counter.class);

        updateOperations.inc("currentCount").addToSet("queuedCustomers", getNewQueuedCustomerInstance(type, tokenNum));

        datastore.findAndModify(query, updateOperations);
    }

    public List<Counter> getCounters(int skip, int limit, int id) {
        Query<Counter> query = datastore.createQuery(Counter.class);
        return query.order(Sort.ascending("id")).asList();
    }


    private QueuedCustomer getNewQueuedCustomerInstance(ServiceTypes type, long tokenNum) {
        QueuedCustomer queuedCustomer = new QueuedCustomer();
        queuedCustomer.setTokenNum(tokenNum);
        queuedCustomer.setType(type);
        return queuedCustomer;
    }
}
