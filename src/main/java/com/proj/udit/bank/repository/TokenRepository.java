package com.proj.udit.bank.repository;

import com.proj.udit.bank.domain.Token;
import com.proj.udit.bank.domain.TokenNumber;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.FindAndModifyOptions;
import org.mongodb.morphia.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by uwadhwa on 8/26/17.
 */
@Component
public class TokenRepository {

    private final Datastore datastore;

    private static final int ID = 123;

    @Autowired
    public TokenRepository(Datastore datastore) {
        this.datastore = datastore;
    }


    public Token fetchExistingToken(long tokenNum){
        Query<Token> query = datastore.createQuery(Token.class);
        query.field("tokenId").equal(tokenNum);
        return query.get();
    }

    public long getNewTokeNumber() {
        Query<TokenNumber> query = datastore.createQuery(TokenNumber.class).field("id").equal(ID);
        UpdateOperations<TokenNumber> operations = datastore.createUpdateOperations(TokenNumber.class).inc("tokenNum");

        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true).upsert(true);

        TokenNumber tokenNumber = datastore.findAndModify(query, operations, options);
        return tokenNumber.getTokenNum();
    }

    public long resetTokeNumber() {
        Query<TokenNumber> query = datastore.createQuery(TokenNumber.class).field("id").equal(ID);
        UpdateOperations<TokenNumber> operations = datastore.createUpdateOperations(TokenNumber.class).set("tokenNum",1);

        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true).upsert(true);

        TokenNumber tokenNumber = datastore.findAndModify(query, operations, options);
        return tokenNumber.getTokenNum();
    }

    public void persistToken(Token token){
        datastore.save(token);
    }

}
