package com.proj.udit.bank.repository;

import com.google.common.hash.Hashing;
import com.proj.udit.bank.domain.Credentials;
import com.proj.udit.bank.domain.User;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * Created by uwadhwa on 8/25/17.
 */
@Component
public class UserRepository {

    @Autowired
    private Datastore datastore;

    public User getCustomer(String loginName) {

        Query<User> query = datastore.createQuery(User.class);
        query.field("loginName").equal(loginName);

        return query.get();
    }

    public boolean loginNameExists(String loginName){
        Query<User> query = datastore.createQuery(User.class);
        return query.field("loginName").equal(loginName).count() > 0 ? true : false;
    }

    public void addUser(User user) throws Exception{
        datastore.save(user);
    }
}
