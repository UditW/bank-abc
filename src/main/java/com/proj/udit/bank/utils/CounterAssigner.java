package com.proj.udit.bank.utils;

import com.proj.udit.bank.authentication.AuthorizationManager;
import com.proj.udit.bank.domain.ServiceTypes;
import com.proj.udit.bank.domain.Token;
import com.proj.udit.bank.repository.CounterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by uwadhwa on 8/27/17.
 */
@Component
public class CounterAssigner {

    private final CounterRepository counterRepository;
    private final AuthorizationManager authorizationManager;

    private final static Logger LOGGER = LoggerFactory.getLogger(CounterAssigner.class);

    @Autowired
    public CounterAssigner(CounterRepository counterRepository,
                           AuthorizationManager authorizationManager) {
        this.counterRepository = counterRepository;
        this.authorizationManager = authorizationManager;
    }

    private void assignCounters(Token token) {
        List<ServiceTypes> list = token.getRequestedServices();
        list.parallelStream().forEach((type) -> {
            try {
                counterRepository.assignACounter(authorizationManager.getUserCategory(token.getLoginName()), type, token.getTokenId());
            } catch (Exception e) {
                LOGGER.error("Error seen while assining a counter for tokenNum-" + token.getTokenId() +
                        " Service-" + type + " UserCategory-" + authorizationManager.getUserCategory(token.getLoginName()));
            }
        });
    }

}
