package com.proj.udit.bank.utils;

import com.proj.udit.bank.authentication.AuthorizationManager;
import com.proj.udit.bank.domain.Token;
import com.proj.udit.bank.domain.User;
import com.proj.udit.bank.domain.UserCategory;
import com.proj.udit.bank.repository.CounterRepository;
import com.proj.udit.bank.repository.TokenRepository;
import com.proj.udit.bank.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by uwadhwa on 8/27/17.
 */
@Component
public class TokenPreprocessor {

    private static final LinkedBlockingQueue<Long> NewlyCreatedTokens = new LinkedBlockingQueue<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenPreprocessor.class);
    private static final long THREAD_SLEEP = 14000;
    private static final int MAX_THREADS = 1;

    private final CounterRepository counterRepository;
    private final TokenRepository tokenRepository;
    private final AuthorizationManager authorizationManager;

    public static void enqueueAToken(long tokenNum) throws Exception {
        NewlyCreatedTokens.add(tokenNum);
    }

    public static Long dequeueAToken() throws Exception {
        return NewlyCreatedTokens.remove();
    }

    public static int getTokenCountInQueue() {
        return NewlyCreatedTokens.size();
    }

    @Autowired
    public TokenPreprocessor(CounterRepository counterRepository,
                             TokenRepository tokenRepository,
                             AuthorizationManager authorizationManager) {
        this.counterRepository = counterRepository;
        this.tokenRepository = tokenRepository;
        this.authorizationManager = authorizationManager;
        ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);
        int i = 0;
        while (i < MAX_THREADS) {
            executorService.submit(new TokenQueueConsumer());
            i++;
        }
    }

    class TokenQueueConsumer implements Runnable {

        @Override
        public void run() {
            try {
                while (true) {
                    try {
                        long tokenNum = dequeueAToken();
                        //long tokenNum = 1000;
                        Token token = tokenRepository.fetchExistingToken(tokenNum);
                        if (token == null) {
                            throw new Exception("Unable to fetch token record from DB. TokenNum-" + tokenNum);
                        }
                        allocateCountersForRequestedServices(token);
                    } catch (NoSuchElementException e) {
                        LOGGER.warn("Queue has gone empty. Sleeping thread.", e);
                        Thread.sleep(THREAD_SLEEP);
                    } catch (Exception e) {
                        LOGGER.error("Error seen in the token pre-processing. Sleeping thread.", e);
                        Thread.sleep(THREAD_SLEEP);
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Exiting main token queue processor thread. Issue seen.", e);
            }
        }

        private void allocateCountersForRequestedServices(Token token) {
            token.getRequestedServices().parallelStream().forEach((type) -> {
                try {
                    counterRepository.assignACounter(authorizationManager.getUserCategory(token.getLoginName()), type, token.getTokenId());
                } catch (Exception e) {
                    LOGGER.error("Error while trying to assign a counter for service-" + type +
                            " Token-" + token, e);
                }

            });
        }
    }
}
